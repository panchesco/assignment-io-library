section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.cycle:
		cmp byte[rdi], 0
		jz .end
		inc rax
		inc rdi
		jmp .cycle
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
	ret
	

string_copy:
	.copy:
		xor rax, rax
		mov ah, byte[rdi]
		mov byte[rsi], ah
		inc rdi
		inc rsi
		test rax, rax
		jnz .copy
		ret
		
print_char:
	push rdi
	mov rax, 1
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	syscall
	add rsp, 8
	ret
		
		
print_int:
	mov rax, rdi
	or rax, rax
	jns print_uint
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
		


print_uint:
	push r15
	mov r15, rsp
	mov rax, rdi
	dec rsp
	mov byte[rsp], 0
	.loop:
		xor rdx, rdx
		push r14;
		mov r14, 10
		div r14
		pop r14
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax
		jz .print
		jmp .loop
	.print:
		mov rdi, rsp
		call print_string
		mov rsp, r15
		jmp .return
	.return:
		pop r15
		ret
	
read_char:
    push 0                     
    mov rax, 0                  
    mov rdi, 0                  
    mov rdx, 1                  
    mov rsi, rsp                
    syscall
    pop rax                                  
    ret 

read_word:
	xor rcx, rcx
	.loop:
		push rdi
		push rsi
		push rcx
		call read_char
		pop rcx
		pop rsi
		pop rdi
		
		cmp rax, 0
		je .end
		cmp al, 0x20
		je .space_symbol
		cmp al, 0x9
		je .space_symbol
		cmp al, 0xA
		je .space_symbol
		mov byte[rdi + rcx], al
		inc rcx
		cmp rcx, rsi
		je .buffer_overflow
		jmp .loop

	.space_symbol:
		cmp rcx, 0
		je .loop
		jmp .end
	
	.end:	
		mov byte[rdi + rcx], 0
		mov rax, rdi
		mov rdx, rcx
		ret
	.buffer_overflow:
		xor rax, rax
		xor rdx, rdx
		ret
		

parse_uint:
	xor rax, rax
	push r12
	xor r12, r12
	mov r11, 10
	mov r9, 0
	.loop:
		cmp byte[rdi], '0'
		jb .end
		cmp byte[rdi], '9'
		ja .end
		mul r11
		mov r12b, byte[rdi]
		sub r12b, '0'
		add rax, r12
		inc rdi
		inc r9
		jmp .loop
	.end:	
		mov rdx, r9
		pop r12
		ret

parse_int:
	cmp byte[rdi], '-'
	je .neg
	call parse_uint
	jmp .end
	
	.neg:
		inc rdi
		call parse_uint
		cmp rdx, 0
		je .end
		neg rax
		inc rdx
		
	.end:
		ret
		
string_equals:
	.loop:
	mov al, byte[rsi]
	mov dl, byte[rdi]
	cmp al, dl
	jne .ne
	cmp dl, 0
	je .eq
	inc rsi
	inc rdi
	jmp .loop
	
	.eq:
		mov rax, 1
		ret
	
	.ne:
		xor rax, rax
		ret
